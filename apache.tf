resource "null_resource" "provision-apache-server" {
  depends_on = [time_sleep.wait-for-ec2-to-start]

  connection {
    type = "ssh"
    user = "ec2-user"
    private_key = tls_private_key.get-private-key.private_key_pem
    host = aws_instance.get-apache-webserver.public_ip
    timeout = "5m"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo amazon-linux-extras install -y php7.2",
      "sudo yum install -y httpd",
      "sudo systemctl start httpd"
    ]
  }
}

resource "time_sleep" "wait-for-ec2-to-start" {
  depends_on = [aws_instance.get-apache-webserver]

  create_duration = "2m"
}